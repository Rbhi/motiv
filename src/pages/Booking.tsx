import { useState, useEffect } from "react"
import axios from "axios"
import { BookingIconOne, BookingIconTwo } from "../assets/icons"
import CarCard from "../components/cards/CarCard"
import Select from "../components/Select"
import Loader from "../components/Loader"
import Message from "../components/Message"
import { Pagination } from "flowbite-react"

const CHOICES = ["All", "New", "Used"]
const MODELS = [
  "Alfa Romeo",
  "Jeep",
  "Toyota",
  "Audi",
  "Avanti",
  "BMW",
  "Cadillac",
  "Chevrolet",
  "Ford",
  "Honda",
]

interface Car {
  id: number
  brand: string
  model: string
  coupe: string
  image: string
  places: number
  speed_box: string
  price_day: number
  new: boolean
}

const Cars = () => {
  const [carsList, setCarsList] = useState<Car[] | undefined>()
  const [carsFilterd, setCarsFilterd] = useState<Car[] | undefined>()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const [totalPages, setTotalPages] = useState(1)
  const [currentPage, setCurrentPage] = useState(1)
  const [brand, setBrand] = useState("")
  const [newCar, setNewCar] = useState<boolean | null>(null)

  const handleSelect = (choice: string) => {
    const carSelect = choice === "All" ? null : choice === "New" ? true : false
    setNewCar(carSelect)
  }
  const handleBrandSelect = (choice: string) => {
    setBrand(choice)
  }

  useEffect(() => {
    let filtred = carsList
    if (brand && newCar != null) {
      filtred = carsList?.filter(
        (car) => car.brand.toLowerCase() === brand.toLowerCase() && car.new === newCar,
      )
    } else if (brand) {
      filtred = carsList?.filter((car) => car.brand.toLowerCase() === brand.toLowerCase())
    } else if (newCar != null) {
      filtred = carsList?.filter((car) => car.new === newCar)
    }
    setCarsFilterd(filtred)
  }, [brand, newCar])

  useEffect(() => {
    if (carsFilterd) {
      setTotalPages(Math.ceil(carsFilterd.length / 12))
    }
  }, [carsFilterd])

  useEffect(() => {
    if (carsList) {
      setCarsFilterd(carsList)
    }
  }, [carsList])

  useEffect(() => {
    const getData = async () => {
      try {
        setLoading(true)
        const res = await axios.get("https://run.mocky.io/v3/abe2a8bd-a4b2-43bc-9efd-560fda5aad73")
        setCarsList(res.data)
        setLoading(false)
      } catch (error) {
        setLoading(false)
        setError(true)
      }
    }
    getData()
  }, [])

  return loading ? (
    <Loader />
  ) : error ? (
    <Message type="error" />
  ) : (
    <section className="py-4 px-8 md:pt-7 md:pr-11 pb-10 md:pl-7 bg-gray-bg min-h-[calc(100vh_-_5rem)]">
      <header className="mb-8">
        <h1 className="mb-8"> Booking </h1>
        <div className="flex justify-between items-center">
          <div className="flex items-center justify-between gap-4">
            <Select choices={CHOICES} cb={handleSelect} labelTxt={"All"} />
            <Select choices={MODELS.sort()} cb={handleBrandSelect} labelTxt={"Brand"} />
          </div>
          <div className="hidden sm:flex items-center justify-between gap-4">
            <BookingIconOne />
            <BookingIconTwo />
          </div>
        </div>
      </header>
      {carsFilterd && (
        <div className="grid grid-cols-12 gap-6">
          {carsFilterd.slice((currentPage - 1) * 12, (currentPage - 1) * 12 + 12).map((car) => (
            <CarCard key={car.id} car={car} />
          ))}
        </div>
      )}
      {totalPages > 1 && (
        <div className="mt-8 flex justify-center">
          <Pagination
            currentPage={currentPage}
            totalPages={totalPages}
            onPageChange={(n: number) => setCurrentPage(n)}
          />
        </div>
      )}
    </section>
  )
}

export default Cars
