import { useState, useEffect } from "react"
import axios from "axios"
import FeatureCard from "../components/cards/FeatureCard"
import { EarnCard, OfferCard } from "../components/cards/OffersCards"
import RecommendCard from "../components/cards/RecommendCard"
import { EnergyTypes } from "../components/enums"
import { Auth, BestRecomandation, BestSelect } from "../components/inrterfaces"
import Message from "../components/Message"
import Loader from "../components/Loader"

const Dashboard = () => {
  const [auth, setAuth] = useState<Auth>()
  const [bestSelect, setBestSelect] = useState<BestSelect>()
  const [bestRecomandation, setBestRecomandation] = useState<BestRecomandation>()
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  useEffect(() => {
    const getData = async () => {
      try {
        setLoading(true)
        const res = await axios.get("https://run.mocky.io/v3/b7ba0151-426a-4462-831c-b88188d1b6d3")

        setAuth(res.data.auth)
        setBestSelect(res.data.best_select)
        setBestRecomandation(res.data.best_recomandation)
        setLoading(false)
      } catch (error) {
        setLoading(false)
        setError(true)
      }
    }
    getData()
  }, [])

  return loading ? (
    <Loader />
  ) : error ? (
    <Message type="error" />
  ) : (
    <section className="py-4 px-8 md:pt-6 md:pr-11 pb-32 md:pl-16 bg-gray-bg min-h-[calc(100vh_-_5rem)]">
      <div className="grid grid-cols-12 gap-8">
        {bestSelect
          ? bestSelect.map((p, i) => <FeatureCard key={i} property={p.property} value={p.value} />)
          : null}
      </div>
      <div className="grid grid-cols-12 gap-5 mt-6 mb-10">
        <OfferCard />
        {auth?.earn ? (
          <div className="col-span-full xl:col-span-5 grid grid-cols-1 sm:grid-cols-2 gap-5">
            <EarnCard
              src={"images/person.svg"}
              earned={auth.earn.badges}
              user_name={auth.user_name}
            />
            <EarnCard src={"images/person-run.svg"} earned={auth?.earn?.points} />
          </div>
        ) : null}
      </div>
      {bestRecomandation ? (
        <div className="grid grid-cols-12 gap-7">
          {bestRecomandation.map((car, index) => (
            <RecommendCard
              key={index}
              percentage={car.recomanded}
              hourPrice={car.hourPrice}
              name={car.model}
              speedBox={car.speedBox}
              range={car.range}
              airConditioning={car.airConditioning}
              energyType={car.energyType as EnergyTypes}
              image={car.image}
              bgIndex={index}
            />
          ))}
        </div>
      ) : null}
    </section>
  )
}

export default Dashboard
