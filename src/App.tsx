import { Switch, Route, Redirect } from "react-router-dom"
import Sidebar from "./components/Sidebar"
import Booking from "./pages/Booking"
import Dashboard from "./pages/Dashboard"
import SearchCars from "./pages/Search"

const App = () => {
  return (
    <>
      <Sidebar />
      <div className="md:ml-56">
        <Switch>
          <Route path="/booking" component={Booking} />
          <Route path={"/search"} component={SearchCars} />
          <Route path="/" component={Dashboard} exact />
          <Redirect from="*" to="" />
        </Switch>
      </div>
    </>
  )
}

export default App
