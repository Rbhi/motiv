import { useState, useEffect } from "react"
import queryString from "query-string"
import { useHistory, useLocation } from "react-router-dom"
import { SearchIcon } from "../assets/icons"

const NavbarInput = () => {
  const history = useHistory()
  const location = useLocation()
  const [keyword, setKeyWord] = useState("")

  const search = location.search
  const values = queryString.parse(search)

  useEffect(() => {
    if (location.pathname !== "/search") {
      setKeyWord("")
    }
  }, [location.pathname])

  useEffect(() => {
    if (location.pathname === "/search") {
      setKeyWord(values.model as string)
    }
  }, [location.pathname])

  const handleSabmitSearch = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (keyword.trim()) {
      history.push(`/search?model=${keyword.trim()}`)
    }
  }
  return (
    <div className="relative flex items-center bg-gray-white-1 py-3 px-3.5 rounded-lg w-full sm:w-auto lg:w-80 order-2 sm:order-none">
      <span className="mr-4">
        <SearchIcon />
      </span>
      <form className="w-full" onSubmit={handleSabmitSearch}>
        <input
          placeholder="Search or type"
          className="bg-transparent border-none text-base leading-snug text-gray-dark-4 w-full font-medium placeholder:gray-dark-4 placeholder:font-medium focus:outline-none focus:ring-0 pl-2 border-l-2 border-[#EF9011]"
          onChange={(e) => setKeyWord(e.target.value)}
          value={keyword}
        />
      </form>
    </div>
  )
}

export default NavbarInput
