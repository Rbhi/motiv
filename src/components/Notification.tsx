import { Dropdown } from "flowbite-react"
import { NotificationIcon } from "../assets/icons"

const Notification = () => {
  return (
    <Dropdown label={<NotificationIcon />} arrowIcon={false} inline={true}>
      <Dropdown.Header>
        <span className="block text-sm font-bold">NOTIFICATIONS</span>
      </Dropdown.Header>
      <Dropdown.Item>
        <span className="max-w-[200px]">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        </span>
      </Dropdown.Item>
      <Dropdown.Item>
        <span className="max-w-[200px]">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        </span>
      </Dropdown.Item>
    </Dropdown>
  )
}

export default Notification
