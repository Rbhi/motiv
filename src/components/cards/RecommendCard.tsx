import { AirConditioningIcon, EneryDieselIcons, RecomandIcon, SpeedBox } from "../../assets/icons"
import { EnergyTypes } from "../enums"

interface RecommendCardProps {
  percentage: number
  hourPrice: number
  range: number
  name: string
  speedBox: boolean
  airConditioning: boolean
  energyType: EnergyTypes
  image: string
  bgIndex: number
}

const RecommendCard = ({
  percentage,
  name,
  hourPrice,
  speedBox,
  range,
  airConditioning,
  energyType,
  image,
  bgIndex,
}: RecommendCardProps) => {
  const bg = bgIndex === 0 ? "bg-[#E1DFA4]" : bgIndex === 1 ? "bg-[#E3ECF1]" : "bg-[#F4E3E5]"
  return (
    <div
      className={`${bg} flex flex-col gap-2.5 rounded-xl py-3.5 px-7 col-span-full md:col-span-6 xl:col-span-4`}
    >
      <header className="flex gap-3">
        <RecomandIcon />
        <h5>{percentage}% Recommend</h5>
      </header>
      <div className="m-auto">
        <img src={`images/${image}.svg`} alt={name} />
      </div>
      <footer>
        <h3 className="mb-2"> {name}</h3>
        <div className="flex justify-between">
          <ul className=" flex items-center gap-4">
            {speedBox && (
              <li>
                <SpeedBox />
              </li>
            )}
            <li className="text-gray-dark-3 text-small font-medium">{range}k</li>
            {airConditioning && (
              <li>
                <AirConditioningIcon />
              </li>
            )}
            <li>{energyType === EnergyTypes.Diesel ? <EneryDieselIcons /> : null}</li>
          </ul>
          <p className="text-gray-dark-3 text-small font-medium">${hourPrice}/h</p>
        </div>
      </footer>
    </div>
  )
}

export default RecommendCard
