import { CircularProgressbar } from "react-circular-progressbar"
import "react-circular-progressbar/dist/styles.css"
import { EnergyIcon, FluidIcon, RangeIcon, WearIcon } from "../../assets/icons"

interface FeatureCardProps {
  property: "energy" | "range" | "breakFluid" | "tireWear"
  value: number
}

const properties = {
  energy: {
    textHeader: "Energy",
    pathColor: "#FFFFFF",
    icon: <EnergyIcon />,
  },
  range: {
    textHeader: "Range",
    pathColor: "#FF7E86",
    icon: <RangeIcon />,
  },
  breakFluid: {
    textHeader: "Break fluid",
    pathColor: "#A162F7",
    icon: <FluidIcon />,
  },
  tireWear: {
    textHeader: "Tire Wear",
    pathColor: "#F6CC0D",
    icon: <WearIcon />,
  },
}

const FeatureCard = ({
  property,
  value,
}: 
FeatureCardProps) => {
  const isEnergy = property === "energy"
  const isRange = property === "range"
  const bg = isEnergy ? "bg-primary-purple" : "bg-white"

  return (
    <section
      className={`${bg} flex flex-col items-center gap-8 rounded-xl py-5 px-12 col-span-full sm:col-span-6 xl:col-span-3`}
    >
      <div className="flex flex-col items-center gap-3">
        {properties[property].icon}
        <h2 className="whitespace-nowrap" style={{ color: !isEnergy ? "#242731" : "#FFFFFF" }}>
          {properties[property].textHeader}
        </h2>
      </div>

      <div className="w-28">
        <CircularProgressbar
          value={!isRange ? value : 100**2/value}
          text={isRange ? `${value}k` : `${value}%`}
          strokeWidth={10}
          circleRatio={0.7}
          styles={{
            path: {
              stroke: properties[property].pathColor,
              transform: "rotate(0.65turn)",
              transformOrigin: "center center",
            },
            trail: {
              stroke: !isEnergy ? "#F4F5F9" : "#B37EFC",
              transform: "rotate(0.65turn)",
              transformOrigin: "center center",
            },
            text: {
              fill: !isEnergy ? "#242731" : "#FFFFFF",
              fontSize: "24px",
              fontWeight: 700,
            },
            background: {
              fill: bg,
            },
          }}
        />
      </div>
    </section>
  )
}
export default FeatureCard
