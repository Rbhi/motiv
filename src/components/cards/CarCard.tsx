import { useState } from "react"
import { BoxSpeedIcon, LikeIcon, PlacesIcon } from "../../assets/icons"

interface CarCardProps {
  car: {
    model: string
    coupe: string
    image: string
    places: number
    speed_box: string
    price_day: number
  }
}

const CarCard = ({ car }: CarCardProps) => {
  const [like, setLike] = useState(false)

  const toggleLike = () => {
    setLike(!like)
  }

  return (
    <div className="bg-white flex flex-col gap-5 rounded-2xl p-6 col-span-full md:col-span-6 xl:col-span-4">
      <header className="flex justify-between">
        <div>
          <h4>{car.model}</h4>
          <p className="text-base font-normal text-gray-dark-3">{car.coupe}</p>
        </div>
        <button className="self-start" onClick={toggleLike}>
          <LikeIcon fill={like ? "#F84F56" : "none"} />
        </button>
      </header>
      <div className="m-auto">
        <img src={`images/cars/${car.image}.svg`} alt={car.model} />
      </div>
      <div className="flex justify-between items-center">
        <div className="flex gap-4 items-center">
          <div className="flex gap-1 items-center">
            <PlacesIcon />
            <span className="text-gray-dark-3 font-normal text-lg">{car.places}</span>
          </div>
          <div className="flex gap-1 items-center">
            <BoxSpeedIcon />
            <span className="text-gray-dark-3 font-normal text-base">{car.speed_box}</span>
          </div>
        </div>
        <div className="flex items-center">
          <span className="text-gray-dark-1 font-bold text-lg">${car.price_day}</span>
          <span className="text-gray-dark-3 font-normal text-base">/d</span>
        </div>
      </div>
    </div>
  )
}

export default CarCard
