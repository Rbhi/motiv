interface EarnCardProps {
  src: string
  earned: number | undefined
  user_name?: string | undefined
}

export const OfferCard = () => (
  <div className="bg-[#FFE0BA] flex items-center justify-around flex-wrap md:flex-nowrap gap-7 rounded p-6 sm:p-0 sm:pl-6 sm:pr-1.5 col-span-full xl:col-span-7">
    <img className="sm:order-2" src="images/laptop.svg" alt="offer" />
    <div className="">
      <h2>Apply for a car loan !</h2>
      <p className="text-base mb-7 mt-2 text-primary-dark">This is a sample of a generated text</p>
      <button className="text-xs bg-primary-dark text-white py-3 px-6 rounded">
        Discover More
      </button>
    </div>
  </div>
)

export const EarnCard = ({ src, earned, user_name }: EarnCardProps) => {
  const bg = user_name ? "bg-[#0F2837]" : "bg-[#6E1946]"
  const gift = user_name ? "Badges" : "Points!"
  const textColor = user_name ? "text-[#FF9619]" : "text-[#FAC39B]"
  return (
    <div className={`${bg} flex flex-col justify-center rounded overflow-hidden p-[1.5rem] pr-2`}>
      <div className="flex mb-1 items-start">
        <h2 className="text-white w-2/4">
          You
          <br /> have <br /> earned <br />
          <span className={`text-[2.5rem] font-bold leading-[3rem] ${textColor}`}>{earned}</span>
          <br /> {gift}!
        </h2>
        <div className="mt-5 md:-mt-1">
          <img className="scale-150 sm:scale-105" src={src} alt={`gift ${user_name}`} />
        </div>
      </div>
      <p className={`text-xs ${textColor}`}>
        {user_name ? `Hooray! Way to go ${user_name}!` : "Redeem and claim your rewards!"}
      </p>
    </div>
  )
}
