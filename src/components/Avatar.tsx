import { Dropdown, Avatar } from "flowbite-react"
import { Auth } from "./inrterfaces"

const UserAvatar = ({ email, user_name }: Auth) => {
  return (
    <Dropdown
      label={<Avatar alt="User profile" img="images/user.png" rounded={true} />}
      arrowIcon={false}
      inline={true}
    >
      <Dropdown.Header>
        <span className="block text-sm">{user_name}</span>
        <span className="block truncate text-sm font-medium">{email}</span>
      </Dropdown.Header>
      <Dropdown.Item>Profile</Dropdown.Item>
      <Dropdown.Item>Settings</Dropdown.Item>
      <Dropdown.Divider />
      <Dropdown.Item>Log out</Dropdown.Item>
    </Dropdown>
  )
}

export default UserAvatar
