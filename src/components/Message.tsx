import { Alert } from "flowbite-react"

interface ErrorProps {
  type: "error" | "warning"
}

const Message = ({ type }: ErrorProps) => {
  return (
    <div className="mt-20 w-[50vw]">
      <Alert color={type === "error" ? "failure" : "warning"}>
        {type === "error" ? <span>OOPS! Something went wrong</span> : <span>No match Found!</span>}
      </Alert>
    </div>
  )
}

export default Message
