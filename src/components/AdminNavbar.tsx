import { CrossIcon, MenuIcon } from "../assets/icons"
import UserAvatar from "./Avatar"
import { Auth } from "./inrterfaces"
import NavbarInput from "./NavbarInput"
import Notification from "./Notification"

interface AdminNavbarProps {
  showSidebar: string
  setShowSidebar: (classname: string) => void
  userData: Auth
}
const AdminNavbar = ({ showSidebar, setShowSidebar, userData }: AdminNavbarProps) => {
  return (
    <nav className="bg-white md:bg-gray-bg md:ml-56">
      <div className="container max-w-full mx-auto flex items-center justify-between">
        <div className="md:hidden mt-4 self-start">
          <button onClick={() => setShowSidebar("left-0")}>
            <MenuIcon />
          </button>
          <div
            className={`absolute top-2 md:hidden ${
              showSidebar === "left-0" ? "left-48" : "-left-56"
            } z-50 transition-all duration-300`}
          >
            <button onClick={() => setShowSidebar("-left-56")}>
              <CrossIcon />
            </button>
          </div>
        </div>

        <div className="flex justify-between items-center w-full bg-white py-4 px-8 flex-wrap">
          <NavbarInput />
          <div className="flex items-center gap-10 order-1 sm:order-none w-full sm:w-auto justify-end mb-11 sm:mb-0">
            <Notification />
            <UserAvatar email={userData.email} user_name={userData.user_name} />
          </div>
        </div>
      </div>
    </nav>
  )
}

export default AdminNavbar
