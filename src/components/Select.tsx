import { useState, useEffect } from "react"
import { useLocation } from "react-router-dom"
import { Dropdown } from "flowbite-react"
import { DropdownArrowIcon } from "../assets/icons"

const Label = ({ label }: { label: string }) => (
  <div className="bg-white border border-white rounded-3xl w-32 min-w-fit py-2 px-5 flex justify-between items-center">
    <span className="text-gray-dark-2 text-base mr-3 min-w-fit whitespace-nowrap">{label}</span>
    <DropdownArrowIcon />
  </div>
)

interface SelectProps {
  choices: string[]
  cb: (choice: string) => void
  labelTxt: string
}

const Select = ({ choices, cb, labelTxt }: SelectProps) => {
  const {search} = useLocation()
  const [label, setLabel] = useState(labelTxt)

  useEffect(() => {
    setLabel(labelTxt)
  }, [search])

  const handleSelect = (c: string) => {
    setLabel(c)
    cb(c)
  }

  return (
    <Dropdown label={<Label label={label} />} arrowIcon={false} inline={true}>
      {choices.map((choice, index) => (
        <Dropdown.Item
          key={index}
          onClick={() => handleSelect(choice)}
          className="text-gray-dark-2 text-base min-w-[100px]"
        >
          {choice}
        </Dropdown.Item>
      ))}
    </Dropdown>
  )
}

export default Select
