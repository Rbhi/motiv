import { NavLink as BaseNavLink } from "react-router-dom"

interface NavLinkProps {
  children: React.ReactNode
  path: string
}

const NavLink = (props: NavLinkProps) => (
  <BaseNavLink
    to={props.path}
    exact
    className="flex items-center gap-2 text-sm text-[#5F6165] font-medium px-2 py-1.5 rounded-md"
    activeClassName="bg-[#F3F5F8]"
  >
    {props.children}
  </BaseNavLink>
)

export default NavLink
