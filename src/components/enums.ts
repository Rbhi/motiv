export enum EnergyTypes {
  Electric = "electric",
  Gas = "gas",
  Diesel = "diesel",
  Hybrid = "hybrid",
}
