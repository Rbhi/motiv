import { Spinner } from "flowbite-react"

const Loader = () => {
  return (
    <div className="w-full flex justify-center">
      <Spinner aria-label="loading" />
    </div>
  )
}

export default Loader
