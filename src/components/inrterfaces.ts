export interface Auth {
  user_name: string
  email: string
  earn?: { badges: number; points: number }
}

export interface BestSelectItem {
  property: "energy" | "range" | "breakFluid" | "tireWear"
  value: number
}
export type BestSelect = BestSelectItem[]

interface RecomandationItem {
  recomanded: number
  image: string
  model: string
  speedBox: boolean
  range: number
  airConditioning: boolean
  energyType: string
  hourPrice: number
}
export type BestRecomandation = RecomandationItem[]
