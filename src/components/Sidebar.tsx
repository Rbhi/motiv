import { useState } from "react"
import { CarIcon, DashboardIcon, LogoutIcon, SettingsIcon } from "../assets/icons"
import AdminNavbar from "./AdminNavbar"
import { Auth } from "./inrterfaces"
import NavLink from "./NavLink"

const auth: Auth = {
  user_name: "Mohammed",
  email: "mohammmed@gmail.com",
}

const Sidebar = () => {
  const [showSidebar, setShowSidebar] = useState("-left-56")

  return (
    <>
      <AdminNavbar showSidebar={showSidebar} setShowSidebar={setShowSidebar} userData={auth} />
      <div
        className={`h-screen fixed top-0 ${showSidebar} md:left-0 overflow-y-auto flex-row flex-nowrap overflow-hidden bg-white w-56 z-10 py-8 px-6 transition-all duration-300`}
      >
        <div className="flex-col items-stretch min-h-full flex-nowrap px-0 relative">
          <a href="#" target="_blank" rel="noreferrer" className="mb-9 w-full flex gap-3">
            <img src="images/logo.svg" alt="logo" />
            <h2>Motiv.</h2>
          </a>
          <div className="flex flex-col">
            <ul className="flex flex-col min-w-full list-none">
              <li className="rounded-md mb-1.5">
                <NavLink path="/">
                  <DashboardIcon />
                  Dashboard
                </NavLink>
              </li>
              <li className="rounded-md">
                <NavLink path="/booking">
                  <CarIcon />
                  Cars
                </NavLink>
              </li>
            </ul>
            <ul className="flex-col min-w-full flex list-none absolute bottom-0">
              <li className="rounded-md mb-6">
                <NavLink path="/settings">
                  <SettingsIcon />
                  Settings
                </NavLink>
              </li>
              <li className="rounded-md">
                <button className="flex items-center gap-2 text-sm text-[#5F6165] w-full font-medium px-2 py-1.5 rounded-md">
                  <LogoutIcon />
                  Log out
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default Sidebar
