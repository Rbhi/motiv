# Motiv.

## Table of Contents

- [Motiv.](#Motiv.)
  - [Table of Contents](#table-of-contents)
  - [Clone](#clone)
  - [Setup](#setup)
  - [Run the App Locally](#run-the-app-locally)
  - [Created By](#created-by)
  - [License](#license)

---

## Clone

- Clone this repo to your local machine using

```shell
$ git clone git@gitlab.com:Rbhi/motiv.git
```

---

## Setup

- Install all the dependencies.

> from the root dir, run the command:

```shell
$ npm install
```

---

## Run the App Locally

> To run the app, `cd` into project root dir.

```shell
$ npm start
```

The app is served on [`localhost:3000`](http://localhost:3000/).

---

## Created By

<a href="https://www.linkedin.com/in/riadh-rabhi/" target="_blank">**Riadh Rabhi**</a>

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2022
