/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "primary-dark": "#242731",
        "primary-gray": "#E0E4E7",
        "primary-purple": "#A162F7",
        "gray-bg": "#F5F5F5",
        "gray-white-1": "#F5F4F6",
        "gray-dark-1": "#1F2128",
        "gray-dark-2": "#5F6165",
        "gray-dark-3": "#72767C",
        "gray-dark-4": "#7C7C8D",
        "gray-dark-6": "#A4A5A6",
      }
    },
  },
  plugins: ["react", "react-hooks", "@typescript-eslint", "prettier", require("flowbite/plugin")],
};
